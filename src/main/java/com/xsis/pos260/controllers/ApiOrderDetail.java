package com.xsis.pos260.controllers;

import java.util.*;

import com.xsis.pos260.models.OrderDetail;
import com.xsis.pos260.repositories.OrderDetailRepo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class ApiOrderDetail {
    
    @Autowired
    private OrderDetailRepo orderDetailRepo;

    @GetMapping("/orderdetail")
    public ResponseEntity<List<OrderDetail>> GetAllOrderDetail()
    {
        try
        {
            List<OrderDetail> orderDetail = this.orderDetailRepo.findAll();
            return new ResponseEntity<>(orderDetail, HttpStatus.OK);
        }

        catch (Exception e)
        {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/orderdetailbyorder/{id}")
    public ResponseEntity<List<OrderDetail>> GetOrderById(@PathVariable("id") Long id)
    {
        try
        {
            List<OrderDetail> orderDetail = this.orderDetailRepo.FindByHeaderId(id);
            return new ResponseEntity<>(orderDetail, HttpStatus.OK);
        }

        catch (Exception e)
        {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping("/orderdetail")
    public ResponseEntity<Object> SaveOrderDetail(@RequestBody OrderDetail orderDetail)
    {
        try
        {
            orderDetail.setCreatedBy("Rian");
            orderDetail.setCreatedOn(new Date());
            this.orderDetailRepo.save(orderDetail);
            return new ResponseEntity<>("Success", HttpStatus.CREATED);
        }

        catch (Exception e)
        {
            return new ResponseEntity<>("Failed", HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/orderdetail/{id}")
    public ResponseEntity<Object> UpdateOrderDetail(@RequestBody OrderDetail orderDetail, @PathVariable("id") Long id)
    {
        Optional<OrderDetail> orderDetailData = this.orderDetailRepo.findById(id);

        if (orderDetailData.isPresent())
        {
            orderDetail.setModifiedBy("Rian");
            orderDetail.setModifiedOn(new Date());
            orderDetail.setId(id);
            this.orderDetailRepo.save(orderDetail);
            ResponseEntity rest = new ResponseEntity<>("Success", HttpStatus.CREATED);
            return rest;
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/orderdetail/{id}")
    public ResponseEntity<Object> DeleteOrderDetail(@PathVariable("id") Long id)
    {
        this.orderDetailRepo.deleteById(id);
        return new ResponseEntity<>("Success", HttpStatus.OK);
    }
    
}
