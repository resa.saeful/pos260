package com.xsis.pos260;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Pos260Application {

	public static void main(String[] args) {

		SpringApplication.run(Pos260Application.class, args);
	}

}
