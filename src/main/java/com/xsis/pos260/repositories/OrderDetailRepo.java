package com.xsis.pos260.repositories;

import java.util.List;

import com.xsis.pos260.models.OrderDetail;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface OrderDetailRepo extends JpaRepository<OrderDetail, Long>{
    @Query("FROM OrderDetail WHERE OrderHeaderId = ?1")
    List<OrderDetail> FindByHeaderId(Long orderHeaderId);
}
