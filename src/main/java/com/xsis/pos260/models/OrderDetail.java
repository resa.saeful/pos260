package com.xsis.pos260.models;

import javax.persistence.*;

import org.hibernate.annotations.Where;

@Entity
@Where(clause="is_delete = false")
@Table(name = "order_detail")
public class OrderDetail extends Common {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long Id;

    @ManyToOne
    @JoinColumn(name = "order_header_id", insertable = false, updatable = false)
    public OrderHeader orderHeader;

    @Column(name = "order_header_id")
    private long OrderHeaderId;

    @ManyToOne
    @JoinColumn(name = "product_id", insertable = false, updatable = false)
    public Product product;

    @Column(name = "product_id")
    private long ProductId;

    @Column(name = "quantity")
    private int Quantity;

    @Column(name = "price")
    private float Price;

    public long getId() {
        return Id;
    }

    public void setId(long id) {
        Id = id;
    }

    public OrderHeader getOrderHeader() {
        return orderHeader;
    }

    public void setOrderHeader(OrderHeader orderHeader) {
        this.orderHeader = orderHeader;
    }

    public long getOrderHeaderId() {
        return OrderHeaderId;
    }

    public void setOrderHeaderId(long orderHeaderId) {
        OrderHeaderId = orderHeaderId;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public long getProductId() {
        return ProductId;
    }

    public void setProductId(long productId) {
        ProductId = productId;
    }

    public int getQuantity() {
        return Quantity;
    }

    public void setQuantity(int quantity) {
        Quantity = quantity;
    }

    public float getPrice() {
        return Price;
    }

    public void setPrice(float price) {
        Price = price;
    }

    


}
