package com.xsis.pos260.repositories;

import com.xsis.pos260.models.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ProductRepo extends JpaRepository<Product, Long> {
    @Query("FROM Product WHERE VariantId = ?1")
    List<Product> FindByVariantId(Long variantId);

    @Query("FROM Product WHERE lower(ProductName) LIKE lower(concat('%',?1,'%') ) ")
    List<Product> SearchProduct(String keyword);
}
