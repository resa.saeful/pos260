package com.xsis.pos260.controllers;

import java.util.*;

import com.xsis.pos260.models.OrderHeader;
import com.xsis.pos260.repositories.OrderHeaderRepo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class ApiOrderHeader {

    @Autowired
    private OrderHeaderRepo orderHeaderRepo;

    @GetMapping(value = "/orderheader")
    public ResponseEntity<List<OrderHeader>> GetAllOrderHeader()
    {
        try
        {
            List<OrderHeader> orderHeader = this.orderHeaderRepo.findAll();
            return new ResponseEntity<>(orderHeader, HttpStatus.OK);
        }

        catch (Exception e)
        {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping(value = "/orderheader/{id}")
    public ResponseEntity<List<OrderHeader>> GetOrderHeaderById(@PathVariable("id") Long id)
    {
        try
        {
            Optional<OrderHeader> orderHeader = this.orderHeaderRepo.findById(id);

            if (orderHeader.isPresent())
            {
                ResponseEntity rest = new ResponseEntity<>(orderHeader, HttpStatus.OK);
                return rest;
            } else {
                return ResponseEntity.notFound().build();
            }
        }

        catch (Exception e)
        {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping(value = "/orderheadergetmaxid")
    public ResponseEntity<Long> GetOrderHeaderMaxId()
    {
        try
        {
            Long orderHeader = this.orderHeaderRepo.GetMaxOrderHeader();
            System.out.print(orderHeader);
            return new ResponseEntity<>(orderHeader, HttpStatus.OK);
        }

        catch (Exception e)
        {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/searchorderheader/{keyword}")
    public ResponseEntity<List<OrderHeader>> SearchOrderHeaderName(@PathVariable("keyword") String keyword)
    {
        if (keyword != null)
        {
            List<OrderHeader> orderHeader = this.orderHeaderRepo.SearchOrderHeader(keyword);
            return new ResponseEntity<>(orderHeader, HttpStatus.OK);
        } else {
            List<OrderHeader> orderHeader = this.orderHeaderRepo.findAll();
            return new ResponseEntity<>(orderHeader, HttpStatus.OK);
        }
    }

    @GetMapping("/orderheadermapped")
    public ResponseEntity<Map<String, Object>> GetAllPage(@RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "5") int size)
    {
        try {
            List<OrderHeader> orderHeader = new ArrayList<>();
            Pageable pagingSort = PageRequest.of(page, size);

            Page<OrderHeader> pageTuts;

            pageTuts = orderHeaderRepo.findAll(pagingSort);

            orderHeader = pageTuts.getContent();

            Map<String, Object> response = new HashMap<>();
            response.put("orderHeader", orderHeader);
            response.put("currentPage", pageTuts.getNumber());
            response.put("totalItems", pageTuts.getTotalElements());
            response.put("totalPages", pageTuts.getTotalPages());

            return new ResponseEntity<>(response, HttpStatus.OK);
        }

        catch (Exception e)
        {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(value = "/orderheader")
    public ResponseEntity<Object> SaveOrderHeader(@RequestBody OrderHeader orderHeader)
    {
        String reference = "" + System.currentTimeMillis();
        orderHeader.setReference(reference);

        try
        {
            orderHeader.setCreatedBy("Rian");
            orderHeader.setCreatedOn(new Date());
            this.orderHeaderRepo.save(orderHeader);
            return new ResponseEntity<>("Success", HttpStatus.CREATED);
        }

        catch (Exception e)
        {
            return new ResponseEntity<>("Failed", HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping(value = "/orderheader/{id}")
    public ResponseEntity<Object> UpdateOrderHeader(@PathVariable("id") Long id, @RequestBody OrderHeader orderHeader)
    {
        Optional<OrderHeader> orderHeaderData = this.orderHeaderRepo.findById(id);

        if (orderHeaderData.isPresent())
        {
            orderHeader.setModifiedBy("Resa");
            orderHeader.setModifiedOn(new Date());
            orderHeader.setId(id);
            this.orderHeaderRepo.save(orderHeader);
            ResponseEntity rest = new ResponseEntity<>("Success", HttpStatus.CREATED);
            return rest;
        } else {
            return ResponseEntity.notFound().build();
        }
    }
    
}
