package com.xsis.pos260.repositories;

import java.util.List;

import com.xsis.pos260.models.OrderHeader;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface OrderHeaderRepo extends JpaRepository<OrderHeader, Long>{
    @Query("SELECT MAX(id) AS maxid FROM OrderHeader ")
    Long GetMaxOrderHeader();

    @Query("FROM OrderHeader WHERE lower(Reference) LIKE lower(concat('%',?1,'%') ) ")
    List<OrderHeader> SearchOrderHeader(String keyword);
}
