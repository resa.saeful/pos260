package com.xsis.pos260.controllers;

import com.xsis.pos260.models.Category;
import com.xsis.pos260.repositories.CategoryRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class ApiCategoryController {

    @Autowired
    private CategoryRepo categoryRepo;


    @GetMapping("/category")
    public ResponseEntity<List<Category>> GetAllCategory() {
        try
        {
            List<Category> category = this.categoryRepo.findAll();
            return new ResponseEntity<>(category, HttpStatus.OK);
        }
        catch (Exception ex)
        {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/category/{id}")
    public ResponseEntity<List<Category>> GetCategoryById(@PathVariable("id") Long id)
    {
        try
        {
            Optional<Category> category = this.categoryRepo.findById(id);
            if (category.isPresent())
            {
                ResponseEntity rest = new ResponseEntity(category, HttpStatus.OK);
                return rest;
            }
            else
            {
                return ResponseEntity.notFound().build();
            }
        }
        catch (Exception ex)
        {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }


    @PostMapping(value = "/category")
    public ResponseEntity<Object> saveCategory(@RequestBody Category category)
    {
        try {
            category.setCreatedBy("Resa");
            category.setCreatedOn(new Date());
            this.categoryRepo.save(category);
            return new ResponseEntity<>("success", HttpStatus.OK);
        }
        catch (Exception ex)
        {
            return new ResponseEntity<>("failed", HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/searchcategory/{keyword}")
    public ResponseEntity<List<Category>> SearchCategoryName(@PathVariable("keyword") String keyword)
    {
        if (keyword != null)
        {
            List<Category> category = this.categoryRepo.SearchCategory(keyword);
            return new ResponseEntity<>(category, HttpStatus.OK);
        } else {
            List<Category> category = this.categoryRepo.findAll();
            return new ResponseEntity<>(category, HttpStatus.OK);
        }
    }

    @GetMapping("/categorymapped")
    public ResponseEntity<Map<String, Object>> GetAllPage(@RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "5") int size)
    {
        try {
            List<Category> category = new ArrayList<>();
            Pageable pagingSort = PageRequest.of(page, size);

            Page<Category> pageTuts;

            pageTuts = categoryRepo.findAll(pagingSort);

            category = pageTuts.getContent();

            Map<String, Object> response = new HashMap<>();
            response.put("category", category);
            response.put("currentPage", pageTuts.getNumber());
            response.put("totalItems", pageTuts.getTotalElements());
            response.put("totalPages", pageTuts.getTotalPages());

            return new ResponseEntity<>(response, HttpStatus.OK);
        }

        catch (Exception e)
        {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/category/{id}")
    public ResponseEntity<Object> UpdateCategory(@RequestBody Category category, @PathVariable("id") Long id)
    {
        try {
            Optional<Category> categoryData = this.categoryRepo.findById(id);

            if (categoryData.isPresent())
            {
                category.setId(id);
                category.setModifiedBy("resa");
                category.setModifiedOn(new Date());
                this.categoryRepo.save(category);
                ResponseEntity rest = new ResponseEntity<>("success", HttpStatus.OK);
                return rest;
            }
            else
            {
                return ResponseEntity.notFound().build();
            }
        }
        catch (Exception ex)
        {
            return new ResponseEntity<>("failed", HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping("/category/{id}")
    public ResponseEntity<Object> DeleteCategory(@RequestBody Category category, @PathVariable("id") Long id)
    {
        try {
            Optional<Category> categoryData = this.categoryRepo.findById(id);

            if (categoryData.isPresent())
            {
                category.setId(id);
                category.setModifiedBy("resa");
                category.setModifiedOn(new Date());
                category.setDelete(true);
                this.categoryRepo.save(category);
                ResponseEntity rest = new ResponseEntity<>("success", HttpStatus.OK);
                return rest;
            }
            else
            {
                return ResponseEntity.notFound().build();
            }
        }
        catch (Exception ex)
        {
            return new ResponseEntity<>("failed", HttpStatus.BAD_REQUEST);
        }
    }
}
