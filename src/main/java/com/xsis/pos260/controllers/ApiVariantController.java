package com.xsis.pos260.controllers;


import com.xsis.pos260.models.Variant;
import com.xsis.pos260.repositories.VariantRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class ApiVariantController {
    @Autowired
    private VariantRepo variantRepo;


    @GetMapping("/variant")
    public ResponseEntity<List<Variant>> GetAllVariant() {
        try
        {
            List<Variant> variant = this.variantRepo.findAll();
            return new ResponseEntity<>(variant, HttpStatus.OK);
        }
        catch (Exception ex)
        {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/variantbycategory/{id}")
    public ResponseEntity<List<Variant>> GetAllVariantByCategoryId(@PathVariable("id") Long id)
    {
        try
        {
            List<Variant> variant = this.variantRepo.FindByCategoryId(id);
            return new ResponseEntity<>(variant, HttpStatus.OK);
        }

        catch (Exception exception)
        {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/variant/{id}")
    public ResponseEntity<List<Variant>> GetVariantById(@PathVariable("id") Long id)
    {
        try
        {
            Optional<Variant> variant = this.variantRepo.findById(id);
            if (variant.isPresent())
            {
                ResponseEntity rest = new ResponseEntity(variant, HttpStatus.OK);
                return rest;
            }
            else
            {
                return ResponseEntity.notFound().build();
            }
        }
        catch (Exception ex)
        {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/searchvariant/{keyword}")
    public ResponseEntity<List<Variant>> SearchVariantName(@PathVariable("keyword") String keyword)
    {
        if (keyword != null)
        {
            List<Variant> variant = this.variantRepo.SearchVariant(keyword);
            return new ResponseEntity<>(variant, HttpStatus.OK);
        } else {
            List<Variant> variant = this.variantRepo.findAll();
            return new ResponseEntity<>(variant, HttpStatus.OK);
        }
    }

    @GetMapping("/variantmapped")
    public ResponseEntity<Map<String, Object>> GetAllPage(@RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "5") int size)
    {
        try {
            List<Variant> variant = new ArrayList<>();
            Pageable pagingSort = PageRequest.of(page, size);

            Page<Variant> pageTuts;

            pageTuts = variantRepo.findAll(pagingSort);

            variant = pageTuts.getContent();

            Map<String, Object> response = new HashMap<>();
            response.put("variant", variant);
            response.put("currentPage", pageTuts.getNumber());
            response.put("totalItems", pageTuts.getTotalElements());
            response.put("totalPages", pageTuts.getTotalPages());

            return new ResponseEntity<>(response, HttpStatus.OK);
        }

        catch (Exception e)
        {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @PostMapping(value = "/variant")
    public ResponseEntity<Object> saveVariant(@RequestBody Variant variant)
    {
        try {
            variant.setCreatedBy("Resa");
            variant.setCreatedOn(new Date());
            this.variantRepo.save(variant);
            return new ResponseEntity<>("success", HttpStatus.OK);
        }
        catch (Exception ex)
        {
            return new ResponseEntity<>("failed", HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/variant/{id}")
    public ResponseEntity<Object> UpdateVariant(@RequestBody Variant variant, @PathVariable("id") Long id)
    {
        try {
            Optional<Variant> variantData = this.variantRepo.findById(id);

            if (variantData.isPresent())
            {
                variant.setId(id);
                variant.setModifiedBy("resa");
                variant.setModifiedOn(new Date());
                this.variantRepo.save(variant);
                ResponseEntity rest = new ResponseEntity<>("success", HttpStatus.OK);
                return rest;
            }
            else
            {
                return ResponseEntity.notFound().build();
            }
        }
        catch (Exception ex)
        {
            return new ResponseEntity<>("failed", HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping("/variant/{id}")
    public ResponseEntity<Object> DeleteVariant(@RequestBody Variant variant, @PathVariable("id") Long id)
    {
        try {
            Optional<Variant> variantData = this.variantRepo.findById(id);

            if (variantData.isPresent())
            {
                variant.setId(id);
                variant.setModifiedBy("resa");
                variant.setModifiedOn(new Date());
                variant.setDelete(true);
                this.variantRepo.save(variant);
                ResponseEntity rest = new ResponseEntity<>("success", HttpStatus.OK);
                return rest;
            }
            else
            {
                return ResponseEntity.notFound().build();
            }
        }
        catch (Exception ex)
        {
            return new ResponseEntity<>("failed", HttpStatus.BAD_REQUEST);
        }
    }
}
