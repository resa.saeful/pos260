package com.xsis.pos260.controllers;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(value = "/variant/")
public class VariantController {
    @GetMapping(value = "index")
    public ModelAndView index()
    {
        ModelAndView view = new ModelAndView("variant/index");
        return view;

    }
}
