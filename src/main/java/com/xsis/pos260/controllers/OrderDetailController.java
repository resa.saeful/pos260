package com.xsis.pos260.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/orderdetail")
public class OrderDetailController {
    @GetMapping(value = "index")
    public ModelAndView index()
    {
        ModelAndView view = new ModelAndView("orderdetail/index");
        return view;
    }
}
