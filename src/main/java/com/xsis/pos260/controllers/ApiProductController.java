package com.xsis.pos260.controllers;

import com.xsis.pos260.models.Common;
import com.xsis.pos260.models.Product;
import com.xsis.pos260.repositories.ProductRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class ApiProductController {
    @Autowired
    private ProductRepo productRepo;


    @GetMapping("/product")
    public ResponseEntity<List<Product>> GetAllProduct() {
        try
        {
            List<Product> product = this.productRepo.findAll();
            return new ResponseEntity<>(product, HttpStatus.OK);
        }
        catch (Exception ex)
        {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/productbyvariant/{id}")
    public ResponseEntity<List<Product>> GetAllProductByVariantId(@PathVariable("id") Long id)
    {
        try
        {
            List<Product> product = this.productRepo.FindByVariantId(id);
            return new ResponseEntity<>(product, HttpStatus.OK);
        }

        catch (Exception exception)
        {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/product/{id}")
    public ResponseEntity<List<Product>> GetProductById(@PathVariable("id") Long id)
    {
        try
        {
            Optional<Product> product = this.productRepo.findById(id);
            if (product.isPresent())
            {
                ResponseEntity rest = new ResponseEntity(product, HttpStatus.OK);
                return rest;
            }
            else
            {
                return ResponseEntity.notFound().build();
            }
        }
        catch (Exception ex)
        {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/searchproduct/{keyword}")
    public ResponseEntity<List<Product>> SearchProductName(@PathVariable("keyword") String keyword)
    {
        if (keyword != null)
        {
            List<Product> product = this.productRepo.SearchProduct(keyword);
            return new ResponseEntity<>(product, HttpStatus.OK);
        } else {
            List<Product> product = this.productRepo.findAll();
            return new ResponseEntity<>(product, HttpStatus.OK);
        }
    }

    @GetMapping("/productmapped")
    public ResponseEntity<Map<String, Object>> GetAllPage(@RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "5") int size)
    {
        try {
            List<Product> product = new ArrayList<>();
            Pageable pagingSort = PageRequest.of(page, size);

            Page<Product> pageTuts;

            pageTuts = productRepo.findAll(pagingSort);

            product = pageTuts.getContent();

            Map<String, Object> response = new HashMap<>();
            response.put("product", product);
            response.put("currentPage", pageTuts.getNumber());
            response.put("totalItems", pageTuts.getTotalElements());
            response.put("totalPages", pageTuts.getTotalPages());

            return new ResponseEntity<>(response, HttpStatus.OK);
        }

        catch (Exception e)
        {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @PostMapping(value = "/product")
    public ResponseEntity<Object> saveProduct(@RequestBody Product product)
    {
        try {
            product.setCreatedBy("Resa");
            product.setCreatedOn(new Date());
            this.productRepo.save(product);
            return new ResponseEntity<>("success", HttpStatus.OK);
        }
        catch (Exception ex)
        {
            return new ResponseEntity<>("failed", HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/product/{id}")
    public ResponseEntity<Object> UpdateVariant(@RequestBody Product product, @PathVariable("id") Long id)
    {
        try {
            Optional<Product> productData = this.productRepo.findById(id);

            if (productData.isPresent())
            {
                product.setId(id);
                product.setModifiedBy("resa");
                product.setModifiedOn(new Date());
                this.productRepo.save(product);
                ResponseEntity rest = new ResponseEntity<>("success", HttpStatus.OK);
                return rest;
            }
            else
            {
                return ResponseEntity.notFound().build();
            }
        }
        catch (Exception ex)
        {
            return new ResponseEntity<>("failed", HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping("/product/{id}")
    public ResponseEntity<Object> DeleteProduct(@RequestBody Product product, @PathVariable("id") Long id)
    {
        try {
            Optional<Product> productData = this.productRepo.findById(id);

            if (productData.isPresent())
            {
                product.setId(id);
                product.setModifiedBy("resa");
                product.setModifiedOn(new Date());
                product.setDelete(true);
                this.productRepo.save(product);
                ResponseEntity rest = new ResponseEntity<>("success", HttpStatus.OK);
                return rest;
            }
            else
            {
                return ResponseEntity.notFound().build();
            }
        }
        catch (Exception ex)
        {
            return new ResponseEntity<>("failed", HttpStatus.BAD_REQUEST);
        }
    }
}
