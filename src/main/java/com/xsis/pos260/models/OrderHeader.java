package com.xsis.pos260.models;

import javax.persistence.*;

import org.hibernate.annotations.Where;

@Entity
@Where(clause = "is_delete = false")
@Table(name = "order_header")
public class OrderHeader extends Common{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long Id;

    @Column(name = "reference")
    private String Reference;

    @Column(name = "Amount")
    private String Amount;

    public long getId() {
        return Id;
    }

    public void setId(long id) {
        Id = id;
    }

    public String getReference() {
        return Reference;
    }

    public void setReference(String reference) {
        Reference = reference;
    }

    public String getAmount() {
        return Amount;
    }

    public void setAmount(String amount) {
        Amount = amount;
    }
    
}
